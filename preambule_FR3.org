#+MACRO: NEWLINE @@latex:\\@@ @@html:<br>@@

#+EMAIL: vincent.szymanski[at]univ-st-etienne.fr
# #+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
# #+OPTIONS: author:t c:nil d:(not "LOGBOOK") date:t
# #+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
# #+OPTIONS: tags:t tasks:t tex:t TeX:t LaTeX:t timestamp:t toc:t todo:t |:t
# if the creator option is t, then use the string from CREATOR or the
# default if that is not set. The string is put at the end of the
# document.
# #+OPTIONS: creator:nil

#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: fr
#+SELECT_TAGS: export

# By default I do not want that source code blocks are evaluated on export. Usually
# I want to evaluate them interactively and retain the original results.
#+PROPERTY: header-args :eval never-export

# #+OPTIONS: texht:t
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [a4paper,12pt]
#+LATEX_CLASS_OPTIONS: [colorlinks=true,urlcolor=violet,linkcolor=violet,citecolor=red]
# #+LATEX_HEADER: \usepackage{minted}
# #+LaTeX_HEADER: \usemintedstyle{tango}

#+LATEX_HEADER: \usepackage[margin=2cm, foot=1cm, top=1.2cm]{geometry}

# show hyperlinks in blue font
#+LATEX_HEADER: \hypersetup{colorlinks=true, linkcolor=blue}

# LATEX_HEADER_EXTRA lines will not be loaded when previewing LaTeX snippets

# package to typeset units in equations
# #+LATEX_HEADER_EXTRA: \usepackage{units}

# package that allows to put longer text parts as comments that are
# not put into the PDF document
# #+LATEX_HEADER_EXTRA: \usepackage{comment}

# Needed for rotating floats, e.g. for placing the sidewaystable
#+LATEX_HEADER_EXTRA: \usepackage{rotfloat}

# lmodern provides Latin Modern Type1 fonts. If this is left
# out, Type3 fonts are used which results in a document from
# which one cannot copy (symbol crap) and that is not searchable
# #+LATEX_HEADER_EXTRA: \usepackage{lmodern}
#+LATEX_HEADER_EXTRA: \usepackage{kpfonts}


# for tables where the text in a cell can flow into the next
# line. Width can be defined
#+LATEX_HEADER_EXTRA: \usepackage{tabularx}

# For using tabularx with tables that can span several pages,
# i.e. like tabularx + longtable together
#+LATEX_HEADER_EXTRA: \usepackage{ltablex}

# booktabs can be used for getting a nicer table style with
# thicker lines on top and on the bottom
#+LATEX_HEADER_EXTRA: \usepackage{booktabs}

# Index creation
#+LATEX_HEADER_EXTRA: \usepackage{makeidx}
#+LATEX_HEADER_EXTRA: \makeindex


#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage{nicefrac}

# #+LATEX_HEADER: \usepackage{listings}

#+LATEX_HEADER: \definecolor{bleu}{cmyk}{0.59,0.11,0,0.59}
#+LATEX_HEADER: \definecolor{vert}{cmyk}{0.78,0,0.74,0.45}


#+LATEX_HEADER: \usepackage{lipsum}
#+LATEX_HEADER: \usepackage{bm}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{shapes,snakes}
#+LATEX_HEADER: \usepgflibrary{arrows}
#+LATEX_HEADER: \usetikzlibrary{positioning}
#+LATEX_HEADER: \usepackage{circuitikz}
#+LATEX_HEADER: \usepackage[free-standing-units]{siunitx}
# #+LATEX_HEADER: \usepackage{verbatim}
#+LATEX_HEADER: \usepackage{pgfplots}
# pour les diagrammes de Bode
#+LATEX_HEADER: \usepackage{bodegraph}

# pour faire des panneaux attention
#+LATEX_HEADER: \usepackage{manfnt}
#+LATEX_HEADER: \usetikzlibrary{calc}
#+LATEX_HEADER: \usetikzlibrary{positioning}

#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{import}

#+LATEX_HEADER: \usepackage[french, english]{babel}
#+LATEX_HEADER: \usepackage[francais]{minitoc}
#+LATEX_HEADER: \usepackage{graphicx}
#+LATEX_HEADER: %\usepackage{pdfsync}
#+LATEX_HEADER: \DeclareGraphicsExtensions{.png,.eps,.ps,.pdf}
#+LATEX_HEADER: \graphicspath{{./images/}}
#+LATEX_HEADER: \usepackage[babel=true,kerning=true]{microtype}
#+LATEX_HEADER: \usepackage{picins}
#+LATEX_HEADER: \usepackage{pifont}
#+LATEX_HEADER: \usepackage{hhline}
#+LATEX_HEADER: \usepackage{fancybox}
#+LATEX_HEADER: \usepackage{ifthen}
#+LATEX_HEADER: %\usepackage{enumitem}
#+LATEX_HEADER: %\usepackage{enumerate}
#+LATEX_HEADER: %\usepackage[pointedenum]{paralist}
#+LATEX_HEADER: \usepackage{shorttoc}
#+LATEX_HEADER: \newcounter{minitocdepth}
#+LATEX_HEADER: \setcounter{minitocdepth}{1}
# pour les exercices et les solutions
# pour que les solutions soient affichée après chaque exercice
#+LATEX_HEADER: \usepackage[nosolutionfiles]{answers}
# pour que les solutions soient affichées à la fin

#+LATEX_HEADER: \usepackage{answers}
#+LATEX_HEADER: \usepackage{manfnt}


#+LATEX: \selectlanguage{french}

# #+LATEX_HEADER: \usepackage{alltt}
# #+LATEX_HEADER: \usepackage{pcr}


# règles pour le document PDF
#+LATEX_HEADER: \hypersetup{
#+LATEX_HEADER:        backref=true,                           % Permet d'ajouter des liens dans
#+LATEX_HEADER:        pagebackref=true,                       % les bibliographies
#+LATEX_HEADER:        hyperindex=true,                        % Ajoute des liens dans les index.
#+LATEX_HEADER:        colorlinks=true,                        % Colorise les liens.
#+LATEX_HEADER:        breaklinks=true,                        % Permet le retour à la ligne dans les liens trop longs.
#+LATEX_HEADER:        urlcolor= violet,                         % Couleur des hyperliens.
#+LATEX_HEADER:        linkcolor= violet,                        % Couleur des liens internes.
#+LATEX_HEADER:        bookmarks=true,                         % Créé des signets pour Acrobat.
#+LATEX_HEADER:        bookmarksopen=true,                     % Si les signets Acrobat sont créés,
#+LATEX_HEADER:                                               % les afficher complètement.
#+LATEX_HEADER:        pdftitle={COURS V. SZYMANSKI},  % Titre du document.
#+LATEX_HEADER:                                                % Informations apparaissant dans
#+LATEX_HEADER:        pdfauthor={Vincent Szymanski},                      % dans les informations du document
#+LATEX_HEADER:        pdfsubject={Cours 2° année DUT}           % sous Acrobat.
#+LATEX_HEADER: }

#+LATEX_HEADER: % pour les encadrements sans centrer
#+LATEX_HEADER:  \newlength{\cadredim}
#+LATEX_HEADER:  \setlength{\cadredim}{3cm}
#+LATEX_HEADER:  \newcommand{\dblcadre}[1]{%
#+LATEX_HEADER:  {\setlength{\fboxrule}{3pt}%
#+LATEX_HEADER:  \setlength{\fboxsep}{2pt}%
#+LATEX_HEADER:  \fbox{\setlength{\fboxrule}{1pt}%
#+LATEX_HEADER:  \setlength{\fboxsep}{20pt}%
#+LATEX_HEADER:  \fbox{\parbox{\cadredim}{#1}}}}}

# pour les encadrements centrés
#+LATEX_HEADER:  \newcommand{\Dblcadre}[1]{%
#+LATEX_HEADER: {\setlength{\fboxrule}{3pt}%
#+LATEX_HEADER: \setlength{\fboxsep}{2pt}%
#+LATEX_HEADER: \fbox{\setlength{\fboxrule}{1pt}%
#+LATEX_HEADER: \setlength{\fboxsep}{20pt}%
#+LATEX_HEADER: \fbox{\begin{Bcenter}%
#+LATEX_HEADER: #1\end{Bcenter}}}}}


# definition des environnements
# #+LATEX_HEADER: \newcounter{theo}[section]
# #+LATEX_HEADER: \newcounter{defi}[section]
# #+LATEX_HEADER: \newcounter{formul}[section]
# #+LATEX_HEADER: \newcounter{quest}[section]
# #+LATEX_HEADER: \newcounter{numeroexo}[chapter]




# pour les notes lacunaires
#+LATEX_HEADER: \def\NotesDuProf{false}
#+LATEX_HEADER: \def\NotesDuProf{true}


#+LATEX_HEADER: \newcommand{\NotesDuProfR}[1]{
#+LATEX_HEADER:   \ifthenelse{\equal{\NotesDuProf}{true}}
#+LATEX_HEADER:         {\textcolor{red}{#1}}
#+LATEX_HEADER:         {\Large \phantom{#1}}
#+LATEX_HEADER: }


#+LATEX_HEADER: \newcommand{\NotesDuProfB}[1] {
#+LATEX_HEADER:  \ifthenelse{\equal{\NotesDuProf}{true}}
#+LATEX_HEADER:        {\textcolor{violet}{#1}}
#+LATEX_HEADER:        {\underline{\phantom{#1}}}
#+LATEX_HEADER: }

#+LATEX_HEADER: \usepackage{fancyvrb}


#+LATEX_HEADER:    \lstset{
#+LATEX_HEADER:    language=C,
#+LATEX_HEADER:    numbers=left,
#+LATEX_HEADER:    stepnumber=1,
#+LATEX_HEADER:    numbersep=10pt,
#+LATEX_HEADER:    tabsize=4,
# #+LATEX_HEADER:    showspaces=false,
# #+LATEX_HEADER:    showstringspaces=false,
# #+LATEX_HEADER:     numberstyle=\tiny\color{gray},
# #+LATEX_HEADER: 	numberstyle=\footnotesize, 
# #+LATEX_HEADER: 	language=[LaTeX]TeX, 
#+LATEX_HEADER: 	backgroundcolor=\color{violet!05!white},
#+LATEX_HEADER: 	frame=shadowbox,%
#+LATEX_HEADER: 	rulesepcolor=\color{violet!20!black},
#+LATEX_HEADER: 	rulecolor=\color{violet!20!black},
#+LATEX_HEADER: 	framexleftmargin=5pt,
#+LATEX_HEADER: 	keywordstyle=\color{vert}\bfseries,
#+LATEX_HEADER: 	basicstyle=\footnotesize \ttfamily,
#+LATEX_HEADER:        columns=flexible,
#+LATEX_HEADER:     showspaces=false,
#+LATEX_HEADER:     keepspaces=false,
#+LATEX_HEADER:     showtabs=false,                  
# #+LATEX_HEADER:     upquote=true,
#+LATEX_HEADER:     commentstyle=\color{green!50!black},
#+LATEX_HEADER:     keywordstyle=\color{violet},
#+LATEX_HEADER:     stringstyle=\color{red},
#+LATEX_HEADER:     basicstyle=\footnotesize,
#+LATEX_HEADER:     breakatwhitespace=false,         
#+LATEX_HEADER:     breaklines=true,
# #+LATEX_HEADER:     captionpos=b,                    
#+LATEX_HEADER:     keepspaces=true                 
# #+LATEX_HEADER:     escapeinside=||,
# #+LATEX_HEADER:     morekeywords={redefineColor,dfrac,setlength,cellspacetoplimit, cellspacebottomlimit,firstline,text,intro,introauthor,chapter, itemclass,exostart,corrstart,AfficheCorriges,columnbreak,titlepic, nbcolindex,indexname,printindex}
#+LATEX_HEADER: }


# #+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{verbatim}{fontsize=\scriptsize}
# pour le symbole attention
#+LATEX_HEADER: \newcommand{\attention}{\textcolor{red}{\textdbend}}

# pour encadrer les r\'{e}sultats
# \newcommand{\result}[1]{\Longrightarrow \boxed{\bf{#1}}}

#+LATEX_HEADER: \newbox\mybox
#+LATEX_HEADER: \tikzstyle{mybox} = [draw=red, fill=violet!05, very thick,
#+LATEX_HEADER:     rectangle, rounded corners, inner sep=10pt, inner ysep=20pt]
#+LATEX_HEADER: \tikzstyle{fancytitle} =[fill=red, text=white, rounded corners]


# I define a useful macro for marking index words
#+BEGIN_EXPORT LaTeX
\DeclareRobustCommand{\myindex}[1]{#1\index{#1}}
#+END_EXPORT

#+BEGIN_EXPORT LaTeX
\tikzstyle{encadre2} = [draw=red, fill=black!05, very thick,
rectangle, rounded corners, inner sep=10pt, inner ysep=20pt]
\tikzstyle{encadre1} = [draw=black, very thick, shading = radial, shading angle = 90,  left color = white, right color = white, rectangle, rounded corners, inner sep=10pt, inner ysep=20pt]
\tikzstyle{encadre1title} =[fill=gray, text=white, rounded corners, shading = axis, left color = black, right color = black!50]
#+END_EXPORT


#+OPTIONS: toc:nil  

# For export to ODT
#+OPTIONS: LaTeX:t
#+OPTIONS: tex:imagemagick
#+OPTIONS: tex:dvipng
#+STARTUP: latexpreview
